import {formatDate} from '@/libs/tools';

const dateTimeFmt = 'yyyy-MM-dd hh:mm:ss';
let tableColumns = {};

tableColumns.columns = (vue) => {
    return [
        {title: '序号', type: 'index', align: 'center', key: 'index', sortable: false, width: 50},
        {title: '来源平台', align: 'center', key: 'PlatformName', sortable: true, width: 120},
        {title: '业务位置', key: 'BusinessPosition', sortable: true, width: 120},
        {title: '日志内容', key: 'Content', tooltip: true, sortable: false},
        {
            title: '生成时间',
            key: 'CreationTime',
            sortable: true,
            render: (h, params) => {
                return h(
                    'div',
                    vue.$refs.tables.$scopedSlots.time(params.row.CreationTime)
                );
            },
            width: 120
        },
        {
            title: '操作',
            sortable: false,
            key: 'action',
            render: (h, params) => {
                return h(
                    'div',
                    vue.$refs.tables.$scopedSlots.action(params.row)
                );
            }
        }
    ];
};

export default tableColumns;

﻿using Common;
using Rabbitmq.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using WebApiService.Core;

namespace MSH.LogSystem
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Logger.Info("准备启动Web版服务！");
            WebApiServiceManage.WebHostStart();
        }

        protected void Application_End()
        {
            WebApiServiceManage.WebHostStop();
        }
    }
}

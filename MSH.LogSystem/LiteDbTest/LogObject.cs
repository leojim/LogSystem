﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteDbTest
{
    public class LogObject
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }
    }
}

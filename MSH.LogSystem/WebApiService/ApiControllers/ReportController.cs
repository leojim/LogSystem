﻿using BusinessLayer.Interface;
using Common;
using Configuration;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiService.Core.ApiControllers
{
    /// <summary>
    /// 报表服务
    /// </summary>
    public class ReportController : ManagerController
    {
        public IReportManager IReportManager { get; set; }

        /// <summary>
        /// 获取今日新增报表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public AjaxReturnInfo QueryTodayCountReport()
        {
            var data = IReportManager.GetLogAddCountReport();
            return new AjaxReturnInfo()
            {
                Data = data,
            };
        }

        /// <summary>
        /// 获取总量报表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public AjaxReturnInfo QueryTotalCountReport()
        {
            var datas = IReportManager.GetTotalLogReport();
            return new AjaxReturnInfo()
            {
                Data = datas,
            };
        }
    }
}

﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BaseManager
    {
        private static IContainer _IContainer;

        private static IContainer IContainer
        {
            get
            {
                if (_IContainer != null) return _IContainer;
                
                var _ContainerBuilder = new ContainerBuilder();
                
                var assembly = Assembly.GetExecutingAssembly();
                _ContainerBuilder.RegisterAssemblyTypes(assembly)
                      .Where(t => t.Namespace.EndsWith("Access") && !t.IsAbstract)
                      .AsImplementedInterfaces()
                      .PropertiesAutowired();

                _IContainer = _ContainerBuilder.Build();

                return _IContainer;
            }
        }

        //protected M GetInstance<M>()
        //    where M : IBaseDao
        //{
        //    var manager = IContainer.Resolve<M>();
        //    return manager;
        //}
    }
}

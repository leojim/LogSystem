﻿using BusinessLayer.Interface;
using DTO;
using ElasticSearchAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class InfoLogManager : IInfoLogManager
    {
        private InfoLogAccess _InfoLogAccess = new InfoLogAccess();
        
        public void DeleteLog(string id)
        {
            _InfoLogAccess.DeleteLog(id);
        }

        public void DeleteLog(List<string> ids)
        {
            _InfoLogAccess.DeleteLog(ids);
        }

        public List<LogInfo> QueryLogInfo(LogQuery logQuery)
        {
            return _InfoLogAccess.QueryLogRequest(logQuery);
        }
    }
}
